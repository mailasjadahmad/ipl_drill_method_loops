const csvParser = require("csv-parser");
const fs = require("fs");

const matchesCsvFilePath =
  "/home/asjad/ubuntu/vs code files/IPL_drill/data/deliveries.csv";

let deliveries = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return deliveries.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops";
      const bestEconomyBowlerResultFilePath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops/best_economy_bowler.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        bestEconomyBowlerResultFilePath,
        JSON.stringify(bestEconomy(deliveries))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

function bestEconomy(deliveriesData) {
  let bowlersSuperOverObj = {};
  for (let key in deliveriesData) {
    let cv = deliveriesData[key];
    let totalRuns =
      Number(cv.wide_runs) + Number(cv.noball_runs) + Number(cv.batsman_runs);
    if (cv.is_super_over == "1") {
      // console.log(cv.is_super_over);
      if (bowlersSuperOverObj.hasOwnProperty(cv.bowler)) {
        // console.log(cv.bowler);
        let prevAdded = totalRuns + Number(bowlersSuperOverObj[cv.bowler].runs);
        let totalBalls = bowlersSuperOverObj[cv.bowler].balls + 1;
        bowlersSuperOverObj[cv.bowler] = { runs: prevAdded, balls: totalBalls };
      } else {
        // console.log(bowlersSuperOverObj)
        bowlersSuperOverObj[cv.bowler] = { runs: totalRuns, balls: 1 };
      }
    }
  }

  let bestEconomyBowler = 1000,
    bowlerName = "";

  for (let key in bowlersSuperOverObj) {
    if (
      bowlersSuperOverObj[key].runs / (bowlersSuperOverObj[key].balls / 6) <
      bestEconomyBowler
    ) {
      bestEconomyBowler =
        bowlersSuperOverObj[key].runs / (bowlersSuperOverObj[key].balls / 6);
      bowlerName = key;
    }
  }
  return bowlerName;
}
