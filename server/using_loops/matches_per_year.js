const csvParser = require("csv-parser");
const fs = require("fs");

const matchesCsvFilePath =
  "/home/asjad/ubuntu/vs code files/IPL_drill/data/matches.csv";

let matches = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops";
      const matchesPerYearResultFilePath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops/matches_per_year.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        matchesPerYearResultFilePath,
        JSON.stringify(mactchesPerYear(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
    fs.writeFile(jsonWriteFilePath, resultData, (error) => {
      if (error) {
        console.log("Error occur during writing data to json file", error);
      } else {
        console.log("Result written to the file successfully");
      }
    });
  };

function mactchesPerYear(matchesData) {
  let result={}
  for(let key in matchesData){
    cv=matchesData[key]
    if (result.hasOwnProperty(cv.season)) {
      result[cv.season]++;
    } else {
      result[cv.season] = 1;
    }

  }
  return result;
  
}

