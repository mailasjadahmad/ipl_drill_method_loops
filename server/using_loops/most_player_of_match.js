const csvParser = require("csv-parser");
const fs = require("fs");

const matchesCsvFilePath =
  "/home/asjad/ubuntu/vs code files/IPL_drill/data/matches.csv";

let matches = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops";
      const mostPlayerOfMatchResultFilePath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops/most_player_of_match.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        mostPlayerOfMatchResultFilePath,
        JSON.stringify(mostPlayerOfTheMatch(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

function mostPlayerOfTheMatch(matchesData) {
  let momObj={}
  for(let key in matchesData){
    let cv=matchesData[key];
    if (momObj.hasOwnProperty(cv.season)) {
      if (momObj[cv.season].hasOwnProperty(cv.player_of_match)) {
        momObj[cv.season][cv.player_of_match]++;
      } else {
        momObj[cv.season][cv.player_of_match] = 1;
      }
    } else {
      momObj[cv.season] = {};
    }

  }
  

  let maxMomObj = {};

  for (let key in momObj) {
    let playerMax = "",
      noMax = 0;
    for (let innerKey in momObj[key]) {
      if (momObj[key][innerKey] > noMax) {
        playerMax = innerKey;
        noMax = momObj[key][innerKey];
      }
    }
    maxMomObj[key] = { [playerMax]: noMax };
  }

  return maxMomObj;
}
