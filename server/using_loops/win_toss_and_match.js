const csvParser = require("csv-parser");
const fs = require("fs");

const matchesCsvFilePath =
  "/home/asjad/ubuntu/vs code files/IPL_drill/data/matches.csv";

let matches = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops";
      const matchesWonAnadTossResultFilePath =
        "/home/asjad/ubuntu/vs code files/IPL_drill/public/output_using_loops/win_toss_and_match.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        matchesWonAnadTossResultFilePath,
        JSON.stringify(mactchesAndTossWonPerTeam(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

function mactchesAndTossWonPerTeam(matchesData) {
  let result = {};
  for (let key in matchesData) {
    cv = matchesData[key];
    if (result.hasOwnProperty(cv.winner)) {
      if (cv.toss_winner == cv.winner) result[cv.winner]++;
    } else if (cv.winner != "") {
      if (cv.toss_winner == cv.winner) result[cv.winner] = 1;
    }
  }
  return result;
}
